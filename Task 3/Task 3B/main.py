import run_model
import imgstrip
import os
import shutil
import argparse

def resolve_image(path = None, save = None, amod = None, hmod = None):
    os.mkdir("./Animals")
    os.mkdir("./Habitats")
    ret_dict = dict()
    imgstrip.extract_images(cc_ani = './Animals',cc_hab = './Habitats',path=path, save = save) 
    animals = os.listdir("./Animals/")
    habitats = os.listdir("./Habitats/")
    path = os.getcwd()
    os.chdir(os.path.join(path,"Animals"))
    for animal in animals:
        value = run_model.extract_labels(a=animal,h=None,amod = amod,hmod = hmod)
        ret_dict.update({animal.split(".")[0]:value[animal]})

    os.chdir(os.path.join(path,"Habitats"))

    for habitat in habitats:
        value = run_model.extract_labels(a=None,h=habitat,amod = amod,hmod = hmod)
        ret_dict.update({habitat.split(".")[0]:value[habitat]})

    os.chdir(path)
    '''
    Comment the following two lines before running to keep the Animal and Habitat folder.
    Make sure the Animals and Habitats folder is deleted before running again.
    '''
    #shutil.rmtree("./Animals")
    #shutil.rmtree("./Habitats")
    
    return ret_dict
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--save",type=str,help="save the output.\n Example : output_<image_no>.(png|jpg)")
    parser.add_argument("--amod", help="Usage : -amod ./animal-model.pth")
    parser.add_argument("--hmod", help="Usage : -hmod ./habitat-model.pth")
    parser.add_argument("path",type=str,help="Path to the input image.\n Example : /path/to/image/.(png|jpg)")
    args = parser.parse_args()
    a = resolve_image(path=args.path, save = args.save, amod = args.amod, hmod = args.hmod)
    print(a)