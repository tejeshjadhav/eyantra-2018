import serial
import time
from datetime import datetime as dt
import argparse
import os
import serial.tools.list_ports as Port
connected = [port.device for port in Port.comports()]

parser = argparse.ArgumentParser(description="Using PySerial to send data serially to UART of 2560")
parser.add_argument('-f','--file', default="example_1.txt", help="text file to be sent")
args = parser.parse_args()

start = dt.now().strftime('%H:%M:%S')
print(start)

serialPort = serial.Serial()
serialPort.port = connected[0]
serialPort.baudrate = 9600
serialPort.bytesize = 8 #serial.EIGHTBITS
serialPort.parity = 'N' #serial.PARITY_NONE
serialPort.stopbits = 1 #serial.STOPBITS_ONE
serialPort.timeout = 1
serialPort.write_timeout = 2

try:
    serialPort.open()
except Exception as err:
    print("error. Open the serial port: "+ str(err))
    exit()
hash = '\x23'
if serialPort.isOpen():
    with open(args.file) as f:
        for line in f:
            for ch in line:
                # if ch == '#':
                #    ch = ch.replace('#','')
                time.sleep(0.1)
                serialPort.write(ch.encode())
                # Uncomment these lines to see the values echoed back by UART
                # out = ""
                # while serialPort.inWaiting() > 0:
                #     out+=serialPort.readline().decode('Ascii')
                # if out != "":
                #     print(out)
        time.sleep(0.1)
        hash = hash.encode()
        serialPort.write(hash)
        f.seek(0)            
        print(repr(f.read() + ' #'))
serialPort.close()

"""Not required here."""
# result=[]
# for line in lines.replace('\n',',').replace('#','').split(','):
#     result.append(line.strip())

# def split_list(example_list):
#     halfway = len(example_list)//2
#     A = example_list[:halfway]
#     B = example_list[halfway:]
#     return A, B
    
# habitat, animal = split_list(result)

# print(habitat)
# print(animal)
"""Not required here."""

end = dt.now().strftime('%H:%M:%S')
print(end)    

if __name__ == '__main__':
    pass