#include <stdio.h> 
#include <string.h>
#include<stdlib.h> 
struct coor
{
    int x;
    int y;
};

struct coor animals_destinations[20];
int index = 0;
void parse_string(char *str)
{
    char *token = strtok(str, ","); 
    while (token != NULL) 
    { 
        //printf("%s\n", token); 
        int xaxis = (int)token[0] - 65;
        int yaxis = atoi(&token[1]) - 1;
        animals_destinations[index].x = xaxis;
        animals_destinations[index].y = yaxis;
        index++;
        //printf("(%d,%d) ",xaxis,yaxis);
        token = strtok(NULL, ", "); 
    } 
}

int main() 
{ 
    char str[] = "F2, F6, B1, A4 ";  
    parse_string(str);
    for(int a = 0; a < index; a++)
    {
        printf("(%d,%d) ",animals_destinations[a].x,animals_destinations[a].y);
    }
} 


