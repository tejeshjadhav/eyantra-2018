flag=1

if flag==1:
    with open("./example_1.txt") as f:
        print(repr(f.read()))

    with open("./example_2.txt") as f:
        print(repr(f.read()))    
else:
    with open("./example_1.txt","a+") as f:
        f.write(" #")
        f.seek(0)
        print(repr(f.read()))
    
    with open("./example_2.txt","a+") as f:
        f.write(" #")
        f.seek(0)
        print(repr(f.read()))

if __name__ == "__main__":
    pass
