import torch.nn as nn
import torch.nn.functional as F

class FNet(nn.Module):
    # Extra TODO: Comment the code with docstrings
    """Fruit Net

    """
    def __init__(self):
        # make your convolutional neural network here
        # use regularization
        # batch normalization
        super(FNet, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(3, 6, kernel_size=5,stride=1,padding=2),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(6, 16, kernel_size=5,stride=1,padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.fc1 = nn.Linear(16*25*25, 5) #5 denotes numer of classes
    def forward(self, x):
        # forward propagation
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        #out = F.relu(self.fc1(out))
        out = self.fc1(out)
        return out

if __name__ == "__main__":
    net = FNet()
