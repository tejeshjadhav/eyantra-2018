import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision.utils
import torch.utils.data as data
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import argparse
from PIL import Image

classes_animals = ['arctic fox', 'bear', 'bee', 'butterfly', 'cat', 'cougar', 'cow', 'coyote', 'crab', 'crocodile', 'deer', 'dog', 'eagle', 'elephant', 'fish', 'frog', 'giraffe', 'goat', 'hippo', 'horse', 'kangaroo', 'lion', 'monkey', 'otter', 'panda', 'parrot', 'penguin', 'raccoon', 'rat', 'seal', 'shark', 'sheep', 'skunk', 'snake', 'snow leopard', 'tiger', 'yak', 'zebra']
classes_habitats = ['baseball', 'basketball court', 'beach', 'circular farm', 'cloud', 'commercial area', 'dense residential', 'desert', 'forest', 'golf course', 'harbor', 'island', 'lake', 'meadow', 'medium residential area', 'mountain', 'rectangular farm', 'river', 'sea glacier', 'shrubs', 'snowberg', 'sparse residential area', 'thermal power station', 'wetland']

model_path = dict()
model_path['animals'] = '/home/tanmay999/Desktop/eyantra-local/Task 3/Task 3B/Animal_model.pth'
model_path['habitats'] = '/home/tanmay999/Desktop/eyantra-local/Task 3/Task 3B/Habitat_model.pth'  


def extract_labels(a=None,h=None,amod=model_path['animals'],hmod=model_path['habitats']):
    
    data_transform = {'animal' : transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.49085152,0.48522684,0.42626], [0.25572124,0.24598175,0.26219735])
        ]),
        'habitat' : transforms.Compose([
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.3531069, 0.37017843, 0.32700405], [0.18911172, 0.16734481, 0.16406788])
        ])
        }
    
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    models = {x : torchvision.models.resnet18(pretrained=True) for x in['animals','habitats']}
    for key in models:
        for param in models[key].parameters():
            param.requires_grad = False

        # Parameters of newly constructed modules have requires_grad=True by default
        num_ftrs = models[key].fc.in_features

    models['animals'].fc = nn.Linear(num_ftrs, 38)
    models['habitats'].fc = nn.Sequential(
                nn.Linear(num_ftrs, 128),
                nn.Linear(128,24)
                )

    models = {x: models[x].to(device) for x in['animals','habitats']}

    if amod:
        models['animals'].load_state_dict(torch.load(args.amod,map_location=device))
        
    else:
        models['animals'].load_state_dict(torch.load(model_path['animals'],map_location=device))

    if hmod:
        models['habitats'].load_state_dict(torch.load(args.hmod,map_location=device))
        
    else:
        models['habitats'].load_state_dict(torch.load(model_path['habitats'],map_location=device))

    models['animals'].eval()
    models['habitats'].eval()
    ret_dict = dict()
    if a:
        inputs = (data_transform['animal'](Image.open(a))).to(device)
        tf = transforms.ToPILImage()
        im=tf(inputs)
        im.show()
        inputs = torch.unsqueeze(inputs,0)
        inputs = torch.cat((inputs,inputs,inputs,inputs),dim=0)

        outputs = models['animals'](inputs)
        _, preds = torch.max(outputs, 1)
        ret_dict.update({a : classes_animals[preds[0]]})

    if h:
        inputs = (data_transform['habitat'](Image.open(h))).to(device)
        inputs = torch.unsqueeze(inputs,0)
        inputs = torch.cat((inputs,inputs,inputs,inputs),dim=0)
        
        outputs = models['habitats'](inputs)
        _, preds = torch.max(outputs, 1)
        ret_dict.update({h : classes_habitats[preds[0]]})
    return ret_dict

if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("-a", help = "Usage : -a ./animal.jpg")
    parser.add_argument("-h", help = "Usage : -h ./habitat.png")
    parser.add_argument("--amod", help="Usage : -amod ./animal-model.pth")
    parser.add_argument("--hmod", help="Usage : -hmod ./habitat-model.pth")
    args = parser.parse_args()
    op = extract_labels(a=args.a,h=args.h,amod=args.amod,hmod=args.hmod)
    print(op)

