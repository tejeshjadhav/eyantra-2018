# import the necessary packages
import cv2
import numpy as np
import math

# load the shapes image clone it, convert it to grayscale, and

image = cv2.imread("arena_image_2.png")
orig = image.copy()
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


animals = ('A1','A2','A3','A4','A5','A6','4','B1','B6','C1','C6','D1','D6','E1','E6','F1','F2','F3','F4','F5','F6')
habitat = (0, 1, 6, 11, 16, 21, 2, 7, 12, 17, 22, 3, 8, 13, 18, 23, 4, 9, 14, 19, 24, 5, 10, 15, 20, 25)

def grab_contours(cnts):
    # if the length the contours tuple returned by cv2.findContours
    # is '2' then we are using either OpenCV v2.4, v4-beta, or
    # v4-official
    if len(cnts) == 2:
        cnts = cnts[0]

    # if the length of the contours tuple is '3' then we are using
    # either OpenCV v3, v4-pre, or v4-alpha
    elif len(cnts) == 3:
        cnts = cnts[1]

    # otherwise OpenCV has changed their cv2.findContours return
    # signature yet again and I have no idea WTH is going on
    else:
        raise Exception(("Contours tuple must have length 2 or 3, "
            "otherwise OpenCV changed their cv2.findContours return "
            "signature yet again. Refer to OpenCV's documentation "
            "in that case"))

    # return the actual contours array
    return cnts

def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged


def sort_contours(cnts):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(
        *sorted(zip(cnts, boundingBoxes), key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return cnts, boundingBoxes


edged = auto_canny(gray)

# find contours in the edge map
cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = grab_contours(cnts)


(cnts, boundingBoxes) = sort_contours(cnts)
clone = image.copy()

for (i, c) in enumerate(cnts):
    if i != 6:
        x1,y1 = (c[c[:, :, 0].argmin()][0])
        x2,y2 = (c[c[:, :, 0].argmax()][0])
        X1 = math.floor(x1+((x2-x1)/10))
        X2 = math.floor(x2-((x2-x1)/10))
        Y1 = math.floor(y1-((y1-y2)/10))
        Y2 = math.floor(y2+((y1-y2)/10))
        
        X = (x1+x2)/2
        Y = y1+((y1-y2)/5)
        cX = math.floor(X)
        cY = math.floor(Y)
        crop_img = clone[Y1:Y2, X1:X2]
        n_black_pix = np.sum(crop_img != 255)
        n_total_pix = np.sum(crop_img)
        ratio = (n_black_pix/n_total_pix)
        l = animals[i]

        if (ratio > 0.0006):
            cv2.imwrite(l+".png",crop_img)
            cv2.drawContours(clone, [c], -1, (0, 0, 255 ), 2)
            cv2.putText(clone, "{}".format(l), (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    if i == 6:
        x1,y1 = (c[c[:, :, 0].argmin()][0])
        x2,y2 = (c[c[:, :, 0].argmax()][0])
      
        crop_img1 = clone[y1:y2, x1:x2]
        crop_img_edge1 = edged[y1:y2, x1:x2]
        cv2.imwrite("23.png", crop_img1)

        cnts1 = cv2.findContours(crop_img_edge1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts1 = grab_contours(cnts1)


        # loop over the sorting methods
        (cnts1, boundingBoxes1) = sort_contours(cnts1)

        # loop over the sorted contours and label them
        for (i1, c1) in enumerate(cnts1):
            x,y,w,h = cv2.boundingRect(c1)
            cX1 = math.floor((x+x+w)/2)
            cY1 = math.floor((y+y+h)/2)

            if ((i1 > 0) and (i1 < 26)):

                crop_img2 = crop_img1[math.floor(y+(h/100)):math.floor(y+h-(h/100)), math.floor(x+(w/100)):math.floor(x+w-(x/100))] 
              
                n_white_pix1 = np.sum(crop_img2 == 255)
                n_total_pix1 = w*h
                ratio1 = (n_white_pix1/n_total_pix1)
                

                if (ratio1 < 2.55):

                    gray1 = cv2.cvtColor(crop_img2, cv2.COLOR_BGR2GRAY)
                    edged1 = auto_canny(gray1)
                    
                    cnts2 = cv2.findContours(edged1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                    cnts2 = grab_contours(cnts2)
                    j = habitat[i1]

                    for (i2,c2) in enumerate(cnts2):
                    
                        cv2.imwrite(("{}".format(habitat[i1])+".png"),crop_img)
                        cv2.drawContours(crop_img2, [c2], -1, (0, 0, 255), 2)
                        cv2.putText(crop_img1, "{}".format(habitat[i1]), (cX1, cY1), cv2.FONT_HERSHEY_SIMPLEX,1, (0, 255, 255), 2)


#  show the sorted contour image

img = cv2.resize(clone,(800,800),interpolation=cv2.INTER_AREA)
# cv2.imshow("22", crop_img1)
# cv2.imshow("23", clone1)
# cv2.imwrite("23.png", clone1)
cv2.imshow("1", img)

# wait for a keypress
cv2.waitKey(0)
