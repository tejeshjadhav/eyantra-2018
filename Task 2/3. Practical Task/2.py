import numpy as np
import cv2
from matplotlib import pyplot as plt

def grab_contours(cnts):
    # if the length the contours tuple returned by cv2.findContours
    # is '2' then we are using either OpenCV v2.4, v4-beta, or
    # v4-official
    if len(cnts) == 2:
        cnts = cnts[0]

    # if the length of the contours tuple is '3' then we are using
    # either OpenCV v3, v4-pre, or v4-alpha
    elif len(cnts) == 3:
        cnts = cnts[1]

    # otherwise OpenCV has changed their cv2.findContours return
    # signature yet again and I have no idea WTH is going on
    else:
        raise Exception(("Contours tuple must have length 2 or 3, "
                        "otherwise OpenCV changed their cv2.findContours return "
                        "signature yet again. Refer to OpenCV's documentation "
                        "in that case"))

    # return the actual contours array
    return cnts

def sort_contours(cnts):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(
        *sorted(zip(cnts, boundingBoxes), key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return cnts, boundingBoxes



# src = cv2.imread("arena_image_11.png", 1) # read input image
# gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY) # convert to grayscale
# edges = cv2.Canny(gray,100,200)
# blur = cv2.blur(gray, (3, 3)) # blur the image
# ret, thresh = cv2.threshold(blur, 50, 255, cv2.THRESH_BINARY)
# im2, contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# # create hull array for convex hull points
# hull = []
 
# # calculate points for each contour
# for i in range(len(contours)):
#     # creating convex hull object for each contour
#     hull.append(cv2.convexHull(contours[i], False))

# # create an empty black image
# drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)

# # drawing = src.copy()

# # draw contours and hull points
# for i in range(len(contours)):
#     color_contours = (0, 255, 0) # green - color for contours
#     color = (255, 0, 0) # blue - color for convex hull
#     # draw ith contour
#     cv2.drawContours(drawing, contours, i, color_contours, 1, 8, hierarchy)
#     # draw ith convex hull object
#     cv2.drawContours(drawing, hull, i, color, 1, 8)

img = cv2.imread('arena_image_1.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img = cv2.GaussianBlur(gray,(3,3),0)

edges = cv2.Canny(img,100,200)
cv2.imshow("edges",edges)
cv2.waitKey(100)
drawing = np.zeros((gray.shape[0], gray.shape[1], 3), np.uint8)

cnts = cv2.findContours(edges.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
cnts = grab_contours(cnts)
(cnts, boundingBoxes) = sort_contours(cnts)

for (i, c) in enumerate(cnts):

    cv2.drawContours(drawing, [c], -1, (0, 0, 255), 1)
    # cv2.putText(img, "{}".format(i), (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

cv2.imwrite("edgee.png",drawing)
image = cv2.resize(drawing, (800,800))
cv2.imshow("Canny",image)
cv2.waitKey(10000)