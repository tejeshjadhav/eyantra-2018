
# NOTE: You can only use Tensor API of PyTorch

import torch

# Extra TODO: Document with proper docstring
def sigmoid(z):
    """Calculates sigmoid values for tensors
    """
    result =  1.0/(1.0 + torch.exp(-z))
    return result

# Extra TODO: Document with proper docstring
def delta_sigmoid(z):
    """Calculates derivative of sigmoid function
    """
    grad_sigmoid =  sigmoid(z)*(1.0 - sigmoid(z))
    return grad_sigmoid 

# Extra TODO: Document with proper docstring
def softmax(x):
    """Calculates stable softmax (minor difference from normal softmax) values for tensors
    """
    stable_softmax = torch.exp(x - torch.max(x)) 
    stable_softmax = stable_softmax / stable_softmax.sum()
    return stable_softmax

if __name__ == "__main__":
    pass