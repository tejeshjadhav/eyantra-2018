
# Homecoming (eYRC-2018): Task 1A
# Build a Fully Connected 2-Layer Neural Network to Classify Digits

# NOTE: You can only use Tensor API of PyTorch

from nnet import model
import torch
import torchvision
import torchvision.transforms as transforms
# TODO: import torch and torchvision libraries
# We will use torchvision's transforms and datasets


# TODO: Defining torchvision transforms for preprocessing
# TODO: Using torchvision datasets to load MNIST
# TODO: Use torch.utils.data.DataLoader to create loaders for train and test
# NOTE: Use training batch size = 4 in train data loader.

# NOTE: Don't change these settings
device = "cuda:0" if torch.cuda.is_available() else "cpu"
# MNIST dataset
train_dataset = torchvision.datasets.MNIST(root='../../data/',train=True, transform=transforms.ToTensor(),download=True)

test_dataset = torchvision.datasets.MNIST(root='../../data/',train=False, transform=transforms.ToTensor())

# Data loader
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,batch_size=4, shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,batch_size=4, shuffle=True)
# NOTE: Don't change these settings
# Layer size
N_in = 28 * 28 # Input size
N_h1 = 256 # Hidden Layer 1 size
N_h2 = 256 # Hidden Layer 2 size
N_out = 10 # Output size
# Learning rate
lr = 0.001


# init model
net = model.FullyConnected(N_in, N_h1, N_h2, N_out, device=device)

# TODO: Define number of epochs
N_epoch = 5 # Or keep it as is

testiter = iter(test_loader)
image,label = testiter.next()
for i in range(4):
    print(label[i])
    print(image[i].view(-1,28*28).size())

# TODO: Training and Validation Loop
# >>> for n epochs
## >>> for all mini batches
### >>> net.train(...)
## at the end of each training epoch
## >>> net.eval(...)

for epochs in range(N_epoch):
    print('Training for epoch:',epochs,'\n')
    for i,(inputs,labels) in enumerate(train_loader):
        creloss, accuracy, outputs = net.train(inputs.view(-1,28*28), labels, lr, debug=False)
        print ('Epoch [{}/{}], Step [{}], Loss: {:.4f}, Accuracy: {:.2f}%'.format(epochs+1, N_epoch, i+1, creloss, accuracy))
    
    print('Validation for epoch:',epochs,'\n')
    for i,(inputs,labels) in enumerate(test_loader):
        creloss, accuracy, outputs = net.eval(inputs.view(-1,28*28), labels, debug=False)
        print ('Epoch [{}/{}], Step [{}], Loss: {:.4f}, Accuracy: {:.2f}%'.format(epochs+1, N_epoch, i+1, creloss, accuracy))

# print('End of Training and Validation loops')

# TODO: End of Training
# make predictions on randomly selected test examples
# >>> net.predict(...)
testiter = iter(test_loader)
image,label = testiter.next()
'''
exp_outputs = torch.zeros(label.size(0),N_out)
for i in range(label.size(0)):
    exp_outputs.data[i][(label.data[i]).item()] = 1
print(exp_outputs)
#predict the labels

outputs = net.predict(image.view(-1,28*28))
'''

for i in range(4):
    #label[i].item() gives the correct labels
    print('Correct label:',label[i].item(), '   Predicted label:', outputs[1][i])