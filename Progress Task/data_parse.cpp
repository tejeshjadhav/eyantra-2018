#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int x = 0, y = 1;
char curr_dir = 's';
int habitats[25];

void destination_habitats(char *);
//int cells[25][4][2] = {{ {0,0}, {0,1}, {1,0}, {1,1}}, { {1,0}, {1,1}, {2,0}, {2,1}}, { {2,0}, {2,1}, {3,0}, {3,1}}, { {3,0}, {3,1}, {4,0}, {4,1}}, { {4,0}, {4,1}, {5,0}, {5,1}}, { {0,1}, {0,2}, {1,1}, {1,2}}, { {1,1}, {1,2}, {2,1}, {2,2}}, { {2,1}, {2,2}, {3,1}, {3,2}}, { {3,1}, {3,2}, {4,1}, {4,2}}, { {4,1}, {4,2}, {5,1}, {5,2}}, { {0,2}, {0,3}, {1,2}, {1,3}}, { {1,2}, {1,3}, {2,2}, {2,3}}, { {2,2}, {2,3}, {3,2}, {3,3}}, { {3,2}, {3,3}, {4,2}, {4,3}}, { {4,2}, {4,3}, {5,2}, {5,3}}, { {0,3}, {0,4}, {1,3}, {1,4}}, { {1,3}, {1,4}, {2,3}, {2,4}}, { {2,3}, {2,4}, {3,3}, {3,4}}, { {3,3}, {3,4}, {4,3}, {4,4}}, { {4,3}, {4,4}, {5,3}, {5,4}}, { {0,4}, {0,5}, {1,4}, {1,5}}, { {1,4}, {1,5}, {2,4}, {2,5}}, { {2,4}, {2,5}, {3,4}, {3,5}}, { {3,4}, {3,5}, {4,4}, {4,5}}, { {4,4}, {4,5}, {5,4}, {5,5}}};
struct coor
{
    int x;
    int y;
};

struct cell
{
    int pref_node;
    int cor[4][2];
};

struct animal
{
    char name[3];
    int cor[1][2];
};


struct animal animals[20];
struct cell cells[25];
struct coor animals_destinations[20];
struct coor curr,dest;

void def_animals()
{

    for(int i=0; i <= 5; i++)
    {
        animals[i].name[0] = (char)65;
        animals[i].name[1] = (char) 49 + i;
    }
    strcpy(animals[6].name, "B6");
    strcpy(animals[7].name, "C6");
    strcpy(animals[8].name, "D6");
    strcpy(animals[9].name, "E6");    
    for(int i=10; i <= 15; i++)
    {
        animals[i].name[0] = (char)70;
        animals[i].name[1] = (char) 48 + (16 - i);
    }
    strcpy(animals[16].name, "E1");     
    strcpy(animals[17].name, "D1");
    strcpy(animals[18].name, "C1");
    strcpy(animals[19].name, "B1"); 
}


void def_cells()
{
    int a = 0,b = 0;
    int count = 0;
    for(b = 0; b < 5; b++)
    {
        for(a = 0; a < 5; a++ )
        {
            
            cells[count].cor[0][x] = a;
            cells[count].cor[0][y] = b;
            cells[count].cor[1][x] = a;
            cells[count].cor[1][y] = b + 1;
            cells[count].cor[2][x] = a + 1;
            cells[count].cor[2][y] = b;
            cells[count].cor[3][x] = a + 1;
            cells[count].cor[3][y] = b + 1;
            count++;
        }
    }
}

void print()
{
    for(int w = 0; w < 20; w++)
    printf("%s, ",animals[w].name);
    printf("\n");
    for(int q = 0; q < 25; q++)
    {
        printf(" {");
        for(int f = 0; f < 4; f++)
        {
                if(f != 3)
                    printf(" {%d,%d},",cells[q].cor[f][x],cells[q].cor[f][y]);
                else
                {
                    printf(" {%d,%d}",cells[q].cor[f][x],cells[q].cor[f][y]);

                }
                
        }
        printf("},");
    }
}

void destination_habitats(char *inp)
{
    int index = 0;
    for(int i=0;inp[i] != '\0'; i++)
    {
        if(i == 0)
        {
            habitats[index++] = atoi(inp);
            //printf("%d ",atoi(inp));
        }
        else if (inp[i] == ' ') 
        {
            habitats[index++] = atoi(&inp[i+1]);
            //printf("%d ",atoi(&inp[i+1]));
        }
    }
}


void parse_string(char *str)
{
    static int index = 0;
    char *token = strtok(str, ","); 
    while (token != NULL) 
    { 
        //printf("%s\n", token); 
        int xaxis = (int)token[0] - 65;
        int yaxis = atoi(&token[1]) - 1;
        animals_destinations[index].x = xaxis;
        animals_destinations[index].y = yaxis;
        index++;
        //printf("(%d,%d) ",xaxis,yaxis);
        token = strtok(NULL, ", "); 
    } 
}

//TODO: finding nearest node to distination cell.
struct coor nearest(struct coor current, int dest_cell)
{
    struct coor near_node;
    int x,y,dist_diff;
    x = cells[dest_cell-1].cor[0][0];
    y = cells[dest_cell-1].cor[0][1];

    for(int i=0; i<4; i++)
    {
        dist_diff = (abs(current.x-cells[dest_cell-1].cor[i][0]) + abs(current.y-cells[dest_cell-1].cor[i][1])) - (abs(current.x- x) + abs(current.y - y));
        if(dist_diff<0) 
        {
            x = cells[dest_cell-1].cor[i][0];
            y = cells[dest_cell-1].cor[i][1];
            cells[dest_cell-1].pref_node = i;
        }
    }
    near_node.x = x;
    near_node.y = y;
    return near_node;
}

struct coor displacement(struct coor current, struct coor destination)
{
    struct coor temp;
    temp.x = destination.x - current.x;
    temp.y = destination.y - current.y;
    return temp;
}

void dir_transform(char now_dir, char dest_dir)
{
    char arr[5] = "dlsr";
    int rot,diff;
    int now_index,dest_index;
    for(int i=0; i <4; i++)
    {
        
        if(now_dir == arr[i]) now_index = i;
        if(dest_dir == arr[i]) dest_index = i;
    } 
    rot = abs(dest_index - now_index) % 3;
    diff = (dest_index - now_index);
    if(rot<2)
    {
        if(abs(diff) <= 2)
        {
            if(diff < 0) printf("left turn\n");
            else if(diff > 0 ) printf("right turn\n");
            else printf("do nothing\n");
        }
        else
        {
            if(diff > 0) printf("left turn\n");
            else printf("right turn\n");
        }
    }
    else 
    {
       printf("right turn\n");
       printf("right turn\n"); 
    }
    curr_dir = dest_dir;
}

void do_movement(struct coor arr[8])
{
    for(int i=0; i<8; i++)
    {
        if(arr[i].y > 0) dir_transform(curr_dir,'s');
        if(arr[i].y < 0) dir_transform(curr_dir,'d');
        printf("linefoow * %d\n", abs(arr[i].y));
        if(arr[i].x > 0) dir_transform(curr_dir,'r');
        if(arr[i].x < 0) dir_transform(curr_dir,'l');
        printf("linefoow * %d\n", abs(arr[i].x));
    }
}

int main()
{
    
    char animal_str[25],habitat_str[25];
    curr.x = 0;
    curr.y = 0;

    struct coor a[8];
    
    gets(animal_str);
    gets(habitat_str);
    def_cells();
    def_animals();
    parse_string(animal_str);
    destination_habitats(habitat_str);
    for(int i=0; i<8; i+=2)
    {
        struct coor temp;
        
        a[i] = displacement(curr, animals_destinations[i/2]);
        curr = animals_destinations[i/2];

        temp = nearest(curr, habitats[i/2]);
        printf("%d ", a[i].x);
        printf("%d\n", a[i].y);

        a[i+1] = displacement(curr, temp);

        printf("%d ", a[i+1].x);
        printf("%d\n", a[i+1].y);

        curr = temp;
    }
    printf("\n");
    do_movement(a);
    return 0;
}