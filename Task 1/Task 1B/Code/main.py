
# Homecoming (eYRC-2018): Task 1B
# Fruit Classification with a CNN

from model import FNet
from utils import dataset
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
# import required modules

def train_model(dataset_path, debug=False, destination_path='', save=False):
	"""Trains model with set hyper-parameters and provide an option to save the model.

	This function should contain necessary logic to load fruits dataset and train a CNN model on it. It should accept dataset_path which will be path to the dataset directory. You should also specify an option to save the trained model with all parameters. If debug option is specified, it'll print loss and accuracy for all iterations. Returns loss and accuracy for both train and validation sets.

	Args:
		dataset_path (str): Path to the dataset folder. For example, '../Data/fruits/'.
		debug (bool, optional): Prints train, validation loss and accuracy for every iteration. Defaults to False.
		destination_path (str, optional): Destination to save the model file. Defaults to ''.
		save (bool, optional): Saves model if True. Defaults to False.

	Returns:
		loss (torch.tensor): Train loss and validation loss.
		accuracy (torch.tensor): Train accuracy and validation accuracy.
	"""
	# Write your code here
	# The code must follow a similar structure
	# NOTE: Make sure you use torch.device() to use GPU if available
	device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

	transform = transforms.Compose([
				#transforms.RandomApply([transforms.RandomAffine(30, translate=(0.1,0.1), scale=None, shear=20, resample=False, fillcolor=0),
				#transforms.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0.1)],p=0.4),
				transforms.RandomRotation(180),
				transforms.ToTensor()
				])


	df, train_df, test_df = dataset.create_and_load_meta_csv_df(dataset_path, destination_path, split=0.75)
	
	#hyperparameters
	lr = 0.0015
	batch = 4
	epochs = 5

	trainset = dataset.ImageDataset(train_df,transform=transform)
	trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch, shuffle=True)

	testset = dataset.ImageDataset(test_df,transform=transforms.ToTensor())
	testloader = torch.utils.data.DataLoader(testset, batch_size=batch, shuffle=False)

	net = FNet()
	criterion = nn.CrossEntropyLoss()
	optimizer = torch.optim.SGD(net.parameters(), lr)


	for epoch in range(epochs):  # loop over the dataset multiple times
		for i, data in enumerate(trainloader):
			# get the inputs
			inputs, labels = data

			# forward + backward + optimize
			outputs = net(inputs)
			loss = criterion(outputs, labels)

			optimizer.zero_grad()
			loss.backward()
			optimizer.step()

			# print statistic
			total = labels.size(0)
			_, predicted = torch.max(outputs.data, 1)
			correct = (predicted == labels).sum().item()

			
			print('Epoch [{}], Step [{}], Loss: {:.4f}, Accuracy: {:.2f}%'.format(epoch + 1, i + 1, loss.item(),(correct / total) * 100))

	print('Finished Training')
	
	net.eval()  # eval mode (batchnorm uses moving mean/variance instead of mini-batch mean/variance)
	with torch.no_grad():
		total=0
		correct=0
		for images, labels in testloader:
			outputs = net(images)
			_, predicted = torch.max(outputs.data, 1)
			
			total += labels.size(0)
			correct += (predicted == labels).sum().item()
	if(save==True):
		torch.save(net.state_dict(), 'model.ckpt')
	print(correct/total) #print testing accuracy

if __name__ == "__main__":
	train_model('../Data/fruits/', save=True, destination_path='./')
